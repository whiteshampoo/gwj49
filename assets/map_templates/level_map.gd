class_name LevelMap
extends BaseMap

signal breakdown

@onready var F: Finish = null


var additional_maps: Array = Array()
var maps_cleared: int = 0

func _ready() -> void:
	super._ready()
	for child in get_children():
		if child is AdditionalMap:
			additional_maps.append(child)
			child.finish_collected.connect(_additional_finish_collected)
			child.all_finish_collected.connect(_enable_finish)
			continue
		if child is Finish:
			assert(not is_instance_valid(F), "LevelMap can only have 1 Finish")
			F = child
			F.disable()
			F.collected.connect(_finish_collected)
			continue
	assert(is_instance_valid(F), "LevelMap needs exactly 1 Finish")
	
	

func _additional_finish_collected() -> void:
	if not SliceTime.is_stopped():
		return
	SliceTime.start()
	breakdown.emit()


func _finish_collected() -> void:
	Global.config_dict["done_levels"] = max(Global.config_dict["done_levels"], Global.current_level + 1)
	Global.current_level = Global.LEVEL_HUB
	get_tree().change_scene_to_file("res://Level/hubworld.tscn")

func _enable_finish() -> void:
	maps_cleared += 1
	if maps_cleared >= additional_maps.size():
		F.enable()
