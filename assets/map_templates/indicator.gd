@tool
extends Node3D

@export var text: String = "":
	set(x):
		text = x
		if is_instance_valid(Text):
			Text.text = text
@export var texture: Texture = null:
	set(x):
		texture = x
		if is_instance_valid(Sprite):
			Sprite.texture = texture

@export var level: int = 0
@export var min_level: int = -1

@onready var Text: Label3D = $Label3d
@onready var Sprite: Sprite3D = $Sprite3d


func _ready() -> void:
	Text.text = text
	Sprite.texture = texture
	if Engine.is_editor_hint():
		return
	if level == Global.config_dict["done_levels"] or (min_level > -1 and min_level <= Global.config_dict["done_levels"]):
		return
	hide()
	queue_free()
