class_name AdditionalMap
extends BaseMap

signal finish_collected
signal all_finish_collected

var finishes: Array = Array()

func _ready() -> void:
	
	#assert(get_parent().has)
	slice_time = get_parent().slice_time
	block_time = get_parent().block_time
	super._ready()
	for child in get_children():
		if child is Finish:
			finishes.append(child)
			child.collected.connect(_finish_collected.bind(child))


func destruct_all() -> void:
	var cells: Array = get_used_cells()
	if cells.size() > 0:
		set_cell_item(cells[0], -1)
		create_block(cells[0])
		call_deferred("destruct_all")
	else:
		queue_free()

func _finish_collected(finish: Area3D) -> void:
	finish_collected.emit()
	assert(finish in finishes)
	finishes.erase(finish)
	if finishes.size() == 0:
		all_finish_collected.emit()
		unstable_slices = slices.duplicate()
		slices.clear()
		BlockTime.start()

func next_slice(breaking: int) -> void:
	if not SliceTime.is_stopped():
		return
	if breaking >= start.z:
		SliceTime.start()
