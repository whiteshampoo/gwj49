extends GPUParticles3D


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	emitting = false


func fire() -> void:
	emitting = true
	get_tree().create_timer(0.5).timeout.connect(queue_free)
