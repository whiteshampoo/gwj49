class_name BaseMap
extends GridMap

@onready var block_scene: PackedScene = load("res://assets/falling_box.tscn")


@export var block_time: float = 0.1
@export var slice_time: float = 1.0

@onready var BlockTime: Timer = $Block
@onready var SliceTime: Timer = $Slice


var start: Vector3i = Vector3i.ZERO
var end: Vector3i = Vector3i.ZERO
var break_position: int = 0
var slices: Array = Array()
var unstable_slices: Array = Array()

func _ready() -> void:
	BlockTime.wait_time = block_time
	SliceTime.wait_time = slice_time
	calc_dimensions()

func create_block(cell: Vector3i) -> void:
	var block: RigidBody3D = block_scene.instantiate()
	block.position = cell
	get_parent().add_child(block)
	block.apply_central_impulse(Vector3(randf_range(-1.0, 1.0), randf_range(-1.0, 1.0), 0.0))
	block.apply_torque_impulse(Vector3(randf_range(-1.0, 1.0), randf_range(-1.0, 1.0), randf_range(-1.0, 1.0)))

func calc_dimensions() -> void:
	for cell in get_used_cells():
		start.x = min(start.x, cell.x)
		start.y = min(start.y, cell.y)
		start.z = min(start.z, cell.z)
		end.x = max(end.x, cell.x)
		end.y = max(end.y, cell.y)
		end.z = max(end.z, cell.z)
	break_position = start.z
	for z in range(start.z, end.z + 1):
		slices.append(find_slice(z))


func destruct() -> void:
	if slices.size() == 0:
		SliceTime.stop()
		return
	if BlockTime.is_stopped():
		BlockTime.start()
	unstable_slices.append(slices[0])
	slices.remove_at(0)
	break_position += 1
	if is_in_group("AdditionalMaps"):
		return
	get_tree().call_group("AdditionalMaps", "next_slice", break_position)
	



func find_slice(z: int) -> Array:
	var array: Array = Array()
	for x in range(start.x, end.x + 1):
		var tower: PackedVector3Array = find_tower(x, z)
		if tower.size() > 0:
			array.append(tower)
	return array


func find_tower(x: int, z: int) -> PackedVector3Array:
	var tower: PackedVector3Array = PackedVector3Array()
	for y in range(start.y, end.y + 1):
		if get_cell_item(Vector3i(x, y, z)) != -1:
			tower.append(Vector3i(x, y, z))
	return tower


func remove_cell_from_slice(slice: Array) -> Vector3i:
	assert(slice.size() > 0, "slice is empty")
	slice.shuffle()
	assert(slice[0].size() > 0, "tower is empty")
	var cell: Vector3i = slice[0][0]
	slice[0].remove_at(0)
	if slice[0].size() == 0:
		slice.remove_at(0)
	return cell


func _on_block_timeout() -> void:
	for slice in unstable_slices.duplicate():
		if slice.size() == 0:
			unstable_slices.erase(slice)
			continue
		var cell: Vector3i = remove_cell_from_slice(slice)
		set_cell_item(cell, -1)
		create_block(cell)
	if unstable_slices.size() == 0:
		BlockTime.stop()
