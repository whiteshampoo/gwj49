extends Area3D


@export var level_num: int = 0
@export_file("*.tscn") var level_file: String = ""

func _ready() -> void:
	assert(level_num >= 0)
	if Global.config_dict["done_levels"] < level_num:
		queue_free()
	assert(FileAccess.file_exists(level_file))
	

func _on_finish_body_entered(body: Node3D) -> void:
	if not body is Player:
		return
	$Particles.emitting = false
	get_tree().create_timer(1.0).connect("timeout", _timeout)
	collision_layer = 0
	collision_mask = 0
	$Collected.fire()


func _timeout() -> void:
	Global.current_level = level_num
	get_tree().change_scene_to_file(level_file)
