class_name Finish
extends Area3D

signal collected


func disable() -> void:
	monitorable = false
	monitoring = false
	hide()

func enable() -> void:
	monitorable = true
	monitoring = true
	show()


func _on_finish_body_entered(body: Node3D) -> void:
	if not body is Player:
		return
	$Particles.emitting = false
	get_tree().create_timer(2.0).connect("timeout", _timeout)
	collision_layer = 0
	collision_mask = 0
	$Collected.fire()
	if not get_parent().name == "LevelMap":
		return
	body.SoundWin.play_random(0.1)


func _timeout() -> void:
	collected.emit()
	queue_free()
