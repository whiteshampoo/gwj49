@tool
class_name ColorPalette
extends Resource

@export var colors: Array[Color] = [
	Color("#FF6D6A"),
	Color("#EFBE7D"),
	Color("#E9EC6B"),
	Color("#8BD3E6"),
	Color("#E9CDD0"),
	Color("#B1A2CA"),
] 

func get_color(index : int) -> Color:
	assert(index > 0 and index < colors.size(), "index out of bounds")
	return colors[clampi(index, 0, colors.size() - 1)]
