class_name RandomAudioPlayer
extends AudioStreamPlayer

@export var streams: Array[AudioStream] = Array()
@onready var fixed_pitch: float = pitch_scale

func _ready() -> void:
	if streams.size() == 0:
		push_warning("No AudioStreams attached")

func play_random(random_pitch: float = 0.0, average_pitch: float = INF) -> void:
	if streams.size() == 0:
		return
	stop()
	if average_pitch == INF:
		average_pitch = fixed_pitch
	average_pitch += randf_range(-1.0, 1.0) * random_pitch
	pitch_scale = average_pitch
	stream = streams[randi() % streams.size()]
	play()
