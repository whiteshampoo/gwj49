class_name Player
extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 5.0

var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")
var hanging: bool = false 
var hanging_direction: Vector2 = Vector2.ZERO
var last_position: Vector3 = position
var jumped: bool = false
var was_in_air: bool = false
var last_velo_y: float = 0.0

@onready var Visual: AnimatedSprite3D = $Visual
@onready var Hang: Node3D = $Hang
@onready var Disabler: Area3D = $Hang/Disabler
@onready var Coyote: Timer = $Coyote

@onready var SoundWin: RandomAudioPlayer = $Win
@onready var SoundJump: RandomAudioPlayer = $Jump
@onready var SoundLand: RandomAudioPlayer = $Land

func _physics_process(delta):
	
	if is_on_floor():
		Coyote.start()
	else:
		velocity.y -= gravity * delta
	
	if is_on_floor() and was_in_air and last_velo_y < 1.05 * -JUMP_VELOCITY:
		SoundLand.play_random(0.1)
	was_in_air = not is_on_floor()
	last_velo_y = velocity.y
	
	if Input.is_action_just_pressed("jump") and (not Coyote.is_stopped() or hanging):
		velocity.y = JUMP_VELOCITY
		hanging = false
		jumped = true
		Coyote.stop()
		SoundJump.play_random(0.1)
	if not Input.is_action_pressed("jump") and jumped:
			velocity.y /= 2
			jumped = false
	if velocity.y < 0.0:
		jumped = false
		
	var input_dir: Vector2 = Input.get_vector("left", "right", "up", "down")
	var movement_dir: Vector2 = input_dir.rotated(TAU / -8.0)
	var direction: Vector3 = (transform.basis * Vector3(movement_dir.x, 0, movement_dir.y)).normalized()
	if not hanging:
		if input_dir.x < 0.0:
			Visual.flip_h = true
			Visual.offset.x = -4
		elif input_dir.x > 0.0:
			Visual.flip_h = false
			Visual.offset.x = 4
		Visual.offset.y = 0
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
		#var hang_dir: Vector2 = input_dir
		Hang.position.x = velocity.normalized().x * 0.25
		Hang.position.z = velocity.normalized().z * 0.25
	else:
		input_dir = Vector2.ZERO
		velocity = Vector3.ZERO
	
	if hanging:
		Visual.offset.x = -8 if Visual.flip_h == true else 8
		if hanging_direction.x > 0.0 or hanging_direction.y > 0.0:
			Visual.offset.y = -11
	
	Visual.animation = get_animation()
	last_position = position
	move_and_slide()
	if position.y < -5.0:
		get_tree().reload_current_scene()

func get_animation() -> String:
	if hanging:
		return "hang"
	if is_on_floor():
		if is_zero_approx(velocity.length_squared()):
			return "idle"
		return "run"
#	if Visual.animation_finished.is_connected(_fall):
#		return "float"
#	if last_position.y > position.y and jumped:
#		print("float")
#		Visual.frame = 0
#		Visual.animation_finished.connect(_fall, CONNECT_ONE_SHOT)
#		return "float"
	if velocity.y > 0.0:
		return "jump"
	return "fall"

#func _fall() -> void:
#	print("x")
#	Visual.animation = "fall"
#	jumped = false


func _on_enabler_body_entered(body: Node3D) -> void:
	if velocity.y < 0.0 and not is_on_floor() and not body in Disabler.get_overlapping_bodies():
		hanging = true
		hanging_direction = Vector2(Hang.position.x, Hang.position.z).normalized()



func _on_enabler_body_exited(_body: Node3D) -> void:
	if hanging:
		hanging = false
