extends RigidBody3D

func _ready() -> void:
	$AudioStreamPlayer3d.play_random(0.1)
	get_tree().create_timer(0.5).timeout.connect(_collision_timeout)

func _physics_process(_delta: float) -> void:
	if position.y < -10.0:
		queue_free()


func _collision_timeout() -> void:
	collision_layer = 0
	collision_mask = 0
