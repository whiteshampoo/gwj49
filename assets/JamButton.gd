# Made by Benedikt Wicklein aka whiteshampoo
# https://whiteshampoo.itch.io/
# License: CC0 (Crediting appreciated but not neccesary)
# Backport to 3.x by parapixel: https://gitlab.com/-/snippets/2401569 (version 1.1)
# v1.3

@tool
class_name JamButton
extends Button

var DEFAULT_SCENE: String = ProjectSettings.get_setting("application/run/main_scene")
var DEFAULT_URL: String = "https://godotengine.org"
var GROUP: String = "__JAMBUTTON__"

const EXPORT_USAGE = PROPERTY_USAGE_SCRIPT_VARIABLE | PROPERTY_USAGE_STORAGE | PROPERTY_USAGE_EDITOR

enum MODE {
	QUIT,
	CHANGE_SCENE,
	SHELL_OPEN,
	MODE_MAX,
}

var mode: MODE = MODE.QUIT:
	set(x):
		mode = clamp(x, 0, MODE.MODE_MAX - 1)
		match mode:
			MODE.QUIT:
				scene = ""
				url = ""
			MODE.CHANGE_SCENE:
				scene = DEFAULT_SCENE
				url = ""
			MODE.SHELL_OPEN:
				url = DEFAULT_URL
				scene = ""
		valid = false
		notify_property_list_changed()

var scene: String = "":
	set(x):
		scene = x if mode == MODE.CHANGE_SCENE else ""
		valid = false

var url: String = "":
	set(x):
		url = x if mode == MODE.SHELL_OPEN else ""
		valid = false

var valid: bool = true:
	set(__):
		valid = false
		match mode:
			MODE.QUIT:
				valid = scene == "" and url == ""
			MODE.CHANGE_SCENE:
				valid = (FileAccess.file_exists(scene) and url == "") or OS.has_feature("standalone") # bugfix
			MODE.SHELL_OPEN:
				valid = url.begins_with("https://") and scene == ""

var sound_pressed: AudioStream = null
var sound_focus: AudioStream = null
var audio_bus: String = "Master"


func _get_property_list() -> Array:
	var general: Dictionary = create_property_dict("General", 0, PROPERTY_USAGE_CATEGORY)
	var mode_prop: Dictionary = create_property_dict("mode", typeof(mode), 
		EXPORT_USAGE, "", PROPERTY_HINT_ENUM, "Quit:0,Change Scene:1,Shell Open:2")
	var scene_prop: Dictionary = create_property_dict("scene", typeof(scene),
		EXPORT_USAGE if mode == MODE.CHANGE_SCENE else PROPERTY_USAGE_SCRIPT_VARIABLE, "",
		PROPERTY_HINT_FILE, "*.tscn,*.scn,*.res")
	var url_prop: Dictionary = create_property_dict("url", typeof(url),
		EXPORT_USAGE if mode == MODE.SHELL_OPEN else PROPERTY_USAGE_SCRIPT_VARIABLE)
	var audio: Dictionary = create_property_dict("Audio", 0, PROPERTY_USAGE_CATEGORY)
	var valid_prop: Dictionary = create_property_dict("valid", typeof(valid), EXPORT_USAGE)
	var sound_pressed_prop: Dictionary = create_property_dict("sound_pressed", TYPE_OBJECT,
		EXPORT_USAGE, "AudioStream", PROPERTY_HINT_RESOURCE_TYPE, "AudioStream")
	var sound_focus_prop: Dictionary = create_property_dict("sound_focus", TYPE_OBJECT,
		EXPORT_USAGE, "AudioStream", PROPERTY_HINT_RESOURCE_TYPE, "AudioStream")
	var audio_bus_prop: Dictionary = create_property_dict("audio_bus", typeof(audio_bus),
		EXPORT_USAGE, "", PROPERTY_HINT_ENUM, "")
	for bus_id in AudioServer.bus_count:
		audio_bus_prop["hint_string"] += AudioServer.get_bus_name(bus_id) + ","
	return [
		general,
			mode_prop,
			scene_prop,
			url_prop,
			valid_prop,
		audio,
			sound_pressed_prop,
			sound_focus_prop,
			audio_bus_prop]


func create_property_dict(
		_name: String,
		_type: int,
		_usage: int = PROPERTY_USAGE_SCRIPT_VARIABLE,
		_class_name: String = "",
		_hint: int = PROPERTY_HINT_NONE,
		_hint_string: String = "") -> Dictionary:
	return {
		"name": _name,
		"class_name": _class_name,
		"type": _type,
		"hint": _hint,
		"hint_string": _hint_string,
		"usage": _usage,
	}
 

func _ready() -> void:
	if OS.get_name() == "Web" and mode == MODE.QUIT:
		hide()
		queue_free()
		return
	if not is_valid():
		modulate = Color.RED
		disabled = true
		return
	add_to_group("__JAMBUTTON__")
	activate()


func is_valid() -> bool:
	if not valid:
		push_error("JamButton '%s' is invalid!" % name)
		return false
	return true


func activate() -> void:
	pressed.connect(_button_pressed)
	mouse_entered.connect(grab_focus)
	focus_entered.connect(play_sound.bind(sound_focus))
	

func deactivate() -> void:
	pressed.disconnect(_button_pressed)
	mouse_entered.disconnect(grab_focus)
	focus_entered.disconnect(play_sound.bind(sound_focus))


func play_sound(audio : AudioStream, callable : Callable = Callable()) -> void:
	if not audio:
		return
	
	if AudioServer.get_bus_index(audio_bus) == -1:
		push_error("audio_bus = '%s' is invalid!" % audio_bus)
		return
	
	var audio_player: AudioStreamPlayer = AudioStreamPlayer.new()
	audio_player.bus = audio_bus
	audio_player.stream = audio
	audio_player.finished.connect(audio_player.queue_free)
	
	if callable.is_valid():
		audio_player.finished.connect(callable)

	add_child(audio_player)
	audio_player.play()


func _button_pressed() -> void:
	if not is_valid():
		return
	
	if mode != MODE.SHELL_OPEN:
		get_tree().call_group(GROUP, "deactivate")

	var callable: Callable = Callable()
	match mode:
		MODE.QUIT:
			callable = get_tree().quit
		MODE.CHANGE_SCENE:
			callable = get_tree().change_scene_to_file.bind(scene)
		MODE.SHELL_OPEN:
			callable = OS.shell_open.bind(url)
	if sound_pressed:
		play_sound(sound_pressed, callable)
	else:
		callable.call()
