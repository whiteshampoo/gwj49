extends Node

signal audio_configured
signal window_configured

const CONFIG_FILE: String = "user://save.dict"
var config_dict: Dictionary = {
	"done_levels": 0,
	"master_volume": 1.0,
	"master_mute": false,
	"music_volume": 1.0,
	"music_mute": false,
	"sounds_volume": 1.0,
	"sounds_mute": false,
	"window_size": DisplayServer.window_get_size(),
	"window_mode": DisplayServer.WINDOW_MODE_WINDOWED
}

var prev_mode: int = DisplayServer.WINDOW_MODE_WINDOWED
var current_level: int = LEVEL_MENU # -2: Menu, -1 Hubworld
const LEVEL_MENU: int = -2
const LEVEL_HUB: int = -1

func _input(event: InputEvent) -> void:
	if not event is InputEventKey:
		return
	if not event.is_pressed():
		return
	if event.is_action_pressed("fullscreen"):
		print("x")
		if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN:
			config_dict["window_mode"] = prev_mode
		else:
			prev_mode = DisplayServer.window_get_mode()
			config_dict["window_mode"] = DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN
		config_window()
	elif event.is_action_pressed("mute"):
		config_dict["master_mute"] = not config_dict["master_mute"]
		config_audio()

func _enter_tree() -> void:
	load_config()
	get_viewport().size_changed.connect(viewport_size_changed)
	config_window()
	config_audio()
	var resolution: Vector2i = DisplayServer.screen_get_size()
	var window: Window = get_tree().get_root()
	window.position = resolution / 2 - window.size / 2
	

func _exit_tree() -> void:
	save_config()


func _ready() -> void:
	Engine.print_error_messages = false
	Credits.add("Development", "Programming", "Everything", "Benedikt Wicklein", "https://whiteshampoo.itch.io/", "")
	Credits.add("Assets", "Sprites", "Warrior-Free Animation set V1.3", "Clembod", "https://clembod.itch.io/warrior-free-animation-set", "")
	Credits.add("Assets", "Sprites", "Godot Animation", "Chase Middleton", "https://twitter.com/SplendidJams/status/1539335072117039105", "")
	Credits.add("Audio", "Music", "Wild Land", "Fato_Shadow", "https://opengameart.org/content/wild-land-soundtrack", "")
	Credits.add("Audio", "Music", "In Summer", "Snabisch","https://opengameart.org/content/in-summer-0", "")
	Credits.add("Audio", "Music", "Bumblebee loop", "jobromedia", "https://opengameart.org/content/bumblebee-loop", "")
	Credits.add("Audio", "Sounds", "Female RPG Voice Starter Pack", "cicifyre", "https://opengameart.org/content/female-rpg-voice-starter-pack", "")
	Credits.add("Audio", "Sounds", "breaking SFX", "rubberduck", "https://opengameart.org/content/75-cc0-breaking-falling-hit-sfx", "")
	Credits.add("Software", "Engine", "Godot Engine", "" , "https://godotengine.org/", "")
	Credits.add("Software", "Tools", "JFXR", "frozenfractal", "https://jfxr.frozenfractal.com/", "")
	Credits.add("Software", "Tools", "Audacity", "", "https://www.audacityteam.org/", "")

func load_config() -> void:
	if not FileAccess.file_exists(CONFIG_FILE):
		return
	var file: FileAccess = FileAccess.open(CONFIG_FILE, FileAccess.READ)
	if not file.is_open():
		print("Cannot open config-file (read)")
		return
	var opened_save: Dictionary = file.get_var() as Dictionary
	if not opened_save is Dictionary:
		print("config-file corrupted")
		return
	for key in opened_save.keys():
		if not key in config_dict:
			continue
		if not typeof(config_dict[key]) == typeof(opened_save[key]):
			print("type error in config-file: %s" % key)
		config_dict[key] = opened_save[key]

func save_config() -> void:
	var file: FileAccess = FileAccess.open(CONFIG_FILE, FileAccess.WRITE)
	if not file.is_open():
		print("Cannot open config-file (write)")
		return
	file.store_var(config_dict)


func set_audio_bus_volume(bus: String, volume: float) -> void:
	var id: int = AudioServer.get_bus_index(bus)
	assert(id >= 0)
	if id < 0:
		return
	volume = clampf(volume, 0.0, 1.0)
	AudioServer.set_bus_volume_db(id, linear_to_db(volume))
	config_dict["%s_volume" % bus.to_lower()] = volume



func set_audio_bus_mute(bus: String, mute: bool) -> void:
	var id: int = AudioServer.get_bus_index(bus)
	assert(id >= 0)
	if id < 0:
		return
	AudioServer.set_bus_mute(id, mute)
	config_dict["%s_mute" % bus.to_lower()] = mute


func get_audio_bus_volume(bus: String) -> float:
	var id: int = AudioServer.get_bus_index(bus)
	assert(id >= 0)
	if id < 0:
		return 0.0
	var volume: float = AudioServer.get_bus_volume_db(id)
	volume = db_to_linear(volume)
	volume = clampf(volume, 0.0, 1.0)
	return volume


func get_audio_bus_mute(bus: String) -> bool:
	var id: int = AudioServer.get_bus_index(bus)
	assert(id >= 0)
	if id < 0:
		return true
	return AudioServer.is_bus_mute(id)


func config_window() -> void:
	DisplayServer.window_set_min_size(Vector2(
		ProjectSettings.get_setting("display/window/size/viewport_width"),
		ProjectSettings.get_setting("display/window/size/viewport_height")))
	DisplayServer.window_set_size(config_dict["window_size"])
	DisplayServer.window_set_mode(config_dict["window_mode"])
	window_configured.emit()

func viewport_size_changed() -> void:
	config_dict["window_size"] = DisplayServer.window_get_size()

func config_audio() -> void:
	set_audio_bus_volume("Master", config_dict["master_volume"])
	set_audio_bus_mute("Master", config_dict["master_mute"])
	set_audio_bus_volume("Music", config_dict["music_volume"])
	set_audio_bus_mute("Music", config_dict["music_mute"])
	set_audio_bus_volume("Sounds", config_dict["sounds_volume"])
	set_audio_bus_mute("Sounds", config_dict["sounds_mute"])
	audio_configured.emit()
