extends Node3D

@onready var ASP: AudioStreamPlayer = $AudioStreamPlayer
@onready var bumblebee: AudioStreamOggVorbis = load("res://assets/music/BumblebeeLoop.ogg")

func _ready() -> void:
	pass
	#if Global.config_dict["done_levels"] > 0 and name == "Hubworld":
	#	$LevelMap/Arrow.hide()
	#	$LevelMap/Arrow.queue_free()

func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		match name:
			"Hubworld":
				Global.current_level = Global.LEVEL_HUB
				get_tree().change_scene_to_file("res://Level/hubworld.tscn")
			_:
				Global.current_level = Global.LEVEL_MENU
				get_tree().change_scene_to_file("res://Menu/Menu.tscn")
	if Input.is_action_just_pressed("reset"):
		get_tree().change_scene_to_file(scene_file_path)


func _on_level_map_breakdown() -> void:
	var T: Tween = create_tween()
	T.tween_property(ASP, "pitch_scale", 0.0001, 1.0).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_QUAD)
	T.finished.connect(_pitch_finished)


func _pitch_finished() -> void:
	ASP.stop()
	ASP.pitch_scale = 1.0
	ASP.stream = bumblebee
	ASP.play()
