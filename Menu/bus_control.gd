@tool
extends HBoxContainer

@export var bus: String = "Master"
@export var label: String = "Master":
	set(x):
		label = x
		if not is_instance_valid(Name):
			Name = get_node_or_null(^"Name")
		if is_instance_valid(Name):
			Name.text = label

@onready var Volume: HSlider = $Volume
@onready var Mute: Button = $Mute
@onready var Name: Label = $Name

func _ready() -> void:
	assert(AudioServer.get_bus_index(bus) >= 0)
	Name.text = label
	if Engine.is_editor_hint():
		return
	Global.audio_configured.connect(_audio_configured)
	_audio_configured()


func _audio_configured() -> void:
	Volume.value = Global.get_audio_bus_volume(bus)
	Mute.button_pressed = Global.get_audio_bus_mute(bus)


func _on_mute_toggled(mute: bool) -> void:
	Global.set_audio_bus_mute(bus, mute)


func _on_volume_value_changed(volume: float) -> void:
	Global.set_audio_bus_volume(bus, volume)
