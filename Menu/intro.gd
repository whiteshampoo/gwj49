extends ColorRect

@onready var Anim: AnimationPlayer = $AnimationPlayer

func _ready() -> void:
	set_scale_mode_viewport()
	Anim.play("intro")


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("skip_intro"):
		skip()


func set_scale_mode_disabled() -> void:
	get_tree().get_root().content_scale_mode = Window.CONTENT_SCALE_MODE_DISABLED


func set_scale_mode_viewport() -> void:
	get_tree().get_root().content_scale_mode = Window.CONTENT_SCALE_MODE_VIEWPORT


func skip() -> void:
	modulate = Color.BLACK
	set_scale_mode_disabled()
	get_tree().change_scene_to_file("res://Menu/Menu.tscn")
