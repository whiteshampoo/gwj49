extends VBoxContainer


@onready var Background: Panel = $Background

func _ready() -> void:
	Background.visible = false

func _on_button_toggled(button_pressed: bool) -> void:
	Background.visible = button_pressed


func _on_button_focus_entered() -> void:
	$Button.release_focus()
