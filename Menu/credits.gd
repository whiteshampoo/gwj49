extends Control


func _ready() -> void:
	%JamButton.grab_focus()
	var append : String = ""
	var credits : Dictionary = Credits.credits
	for group in credits:
		append += "\n\n\n[b]%s[/b]" % group 
		var subs : Dictionary = credits[group]
		for sub in subs:
			append += "\n\n%s\n" % sub
			var entries : Array = subs[sub]
			for entry in entries:
				append += "\n[url=%s]%s[/url]" % [entry["url"], entry["res"]]
				if entry["dev"] != "":
					append += " by %s" % entry["dev"]
				if entry["license"]:
					if Credits.licenses[entry["license"]]:
						append += " ([url=%s]%s[/url])" % [Credits.licenses[entry["license"]], entry["license"]]
					else:
						append += " (%s)" % entry["license"]
	$Text.text %= append


func _on_Text_meta_clicked(meta : String) -> void:
	if not meta.begins_with("https://"):
		return
# warning-ignore:return_value_discarded
	OS.shell_open(meta)
