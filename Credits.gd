extends Node

var credits : Dictionary = Dictionary()
var licenses : Dictionary = Dictionary()

func _init() -> void:
	add_license("", "")
	add_license("CC0", "https://creativecommons.org/choose/zero/")
	

func add_license(license : String, url : String) -> void:
	licenses[license] = url

func add(group : String, sub : String, res_name : String, dev_name : String, url : String, license : String = "") -> void:
	assert(group, "No group specified")
	#assert(sub, "No sub specified")
	assert(res_name, "No ressource specified")
	#assert(dev_name, "No developer specified")
	#assert(url, "No url specified")
	assert(url.begins_with("https://"), "No https URL")
	assert(license in licenses, "No url in licenses found. Use add_license(license, url)")
	if group == "":
		group = "Additional"
	if sub == "":
		sub = "Other"
	if not group in credits:
		credits[group] = Dictionary()
	if not sub in credits[group]:
		credits[group][sub] = Array()
	var entry : Dictionary = {
		"res": res_name,
		"dev": dev_name,
		"url": url,
		"license": license
	}
	credits[group][sub].append(entry)
